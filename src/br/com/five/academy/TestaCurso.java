package br.com.five.academy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestaCurso {

	public static void main(String[] args) {
		
		Curso javaColletions = new Curso("Dominando as coleções do java", "Paulo Silvera");
		
	
		javaColletions.adiciona(new Aula("Trabalhando com LinkedList", 24));
		javaColletions.adiciona(new Aula("Trabalhando com ArrayList", 21));
		
		List<Aula> aulasImutaveis = javaColletions.getAulas();
		System.out.println(javaColletions.getAulas());
		
		
		List<Aula> aulas = new ArrayList<>(aulasImutaveis);
		Collections.sort(aulas);
		System.out.println(javaColletions.getAulas());
		System.out.println(javaColletions.getTempoTotal());
		System.out.println(javaColletions);
	}

}
